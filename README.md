# Project Title

An android native(Java) music and video player.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes as well as deployment on an actual device.


### Prerequisites

JDK: http://www.oracle.com/technetwork/java/javase/overview/index.html
Android Studio: https://developer.android.com/studio/

### Installing
1. Install Android Studio and set-up the Android SDK following instructions on:

```
https://developer.android.com/studio/install
```

2. Import project through file menu

## Running the tests

TODO: Unit test inplementation


## Deployment

1. Add an AVD(Android Virtual Device) or connect your device in USB debug mode.
2. Click Run on the Android Studio toolbar or (Shift+F10)
   Run can also be accessed from the 'Run' main menu

## Versioning

I use [SemVer](http://semver.org/) for versioning.

## Authors

* **Gideon Muiru**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

