package com.orionlabs.euphoria.lastfmapi.callbacks;

public interface UserListener {
    void userSuccess();

    void userInfoFailed();

}
