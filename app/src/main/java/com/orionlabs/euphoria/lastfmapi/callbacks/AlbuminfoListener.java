package com.orionlabs.euphoria.lastfmapi.callbacks;

import com.orionlabs.euphoria.lastfmapi.models.LastfmAlbum;

public interface AlbuminfoListener {

    void albumInfoSucess(LastfmAlbum album);

    void albumInfoFailed();

}
