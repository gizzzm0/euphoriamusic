package com.orionlabs.euphoria.lastfmapi.callbacks;

import com.orionlabs.euphoria.lastfmapi.models.LastfmArtist;

public interface ArtistInfoListener {

    void artistInfoSucess(LastfmArtist artist);

    void artistInfoFailed();

}
