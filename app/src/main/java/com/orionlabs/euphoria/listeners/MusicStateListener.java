package com.orionlabs.euphoria.listeners;

/**
 * Listens for playback changes to send the the fragments bound to this activity
 */
public interface MusicStateListener {

    /**
     * Called when {@link com.orionlabs.euphoria.MusicService#REFRESH} is invoked
     */
    void restartLoader();

    /**
     * Called when {@link com.orionlabs.euphoria.MusicService#PLAYLIST_CHANGED} is invoked
     */
    void onPlaylistChanged();

    /**
     * Called when {@link com.orionlabs.euphoria.MusicService#META_CHANGED} is invoked
     */
    void onMetaChanged();

}
