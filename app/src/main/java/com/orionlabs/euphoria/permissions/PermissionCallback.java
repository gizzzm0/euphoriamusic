package com.orionlabs.euphoria.permissions;

public interface PermissionCallback {
    void permissionGranted();

    void permissionRefused();
}