package com.orionlabs.euphoria.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.orionlabs.euphoria.R;
import com.orionlabs.euphoria.activities.VideoPlayerActivity;
import com.orionlabs.euphoria.models.Video;
import com.orionlabs.euphoria.utils.EuphoriaUtils;
import com.orionlabs.euphoria.utils.PreferencesUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ItemHolder> {

    private List<Video> arrayList;
    private Activity mContext;
    private boolean isGrid;

    public VideosAdapter(Activity context, List<Video> arrayList) {
        this.arrayList = arrayList;
        this.mContext = context;
        this.isGrid = PreferencesUtility.getInstance(mContext).isVideosInGrid();
    }

    @Override
    public VideosAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (isGrid) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_grid, null);
            ItemHolder holder = new ItemHolder(v);
            return holder;
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_list, null);
            ItemHolder holder = new ItemHolder(v);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(final ItemHolder holder, int position) {
        final Video localItem = arrayList.get(position);
        holder.title.setText(localItem.videoTitle);
        holder.duration.setText(EuphoriaUtils.makeShortTimeString(mContext, (localItem.duration) / 1000));

        /*Bitmap thumb = ThumbnailUtils.createVideoThumbnail(localItem.filepath, MediaStore.Video.Thumbnails.MINI_KIND);
        holder.thumbnail.setImageBitmap(thumb);
        */

        Glide.with(mContext)
                .load(Uri.fromFile(new File(localItem.filepath)))
                .into(holder.thumbnail);

        holder.vid_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, VideoPlayerActivity.class);
                intent.putExtra("VID_URL", localItem.filepath);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        protected TextView title, duration;
        protected View vid_content, footer;
        protected ImageView thumbnail;

        public ItemHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.vid_title);
            this.duration = (TextView) itemView.findViewById(R.id.vid_duration);
            this.thumbnail = (ImageView) itemView.findViewById(R.id.vid_art);
            this.footer = itemView.findViewById(R.id.footer);
            this.vid_content = itemView.findViewById(R.id.vid_content);
        }

    }

    public void updateDataSet(ArrayList<Video> arraylist) {
        this.arrayList = arraylist;
    }
}
