package com.orionlabs.euphoria.dataloaders;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.orionlabs.euphoria.models.Video;

import java.util.ArrayList;

public class VideoLoader {
    public static ArrayList<Video> getAllVideos(Context context) {
        return getVideosForCursor(makeVideoCursor(context, null, null));
    }

    public static ArrayList<Video> getVideosForCursor(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        if ((cursor != null) && (cursor.moveToFirst()))
            do {
                long id = cursor.getLong(0);
                String title = cursor.getString(1);
                int duration = cursor.getInt(2);
                String filepath = cursor.getString(3);
                String mimetype  = cursor.getString(4);

                arrayList.add(new Video(id, title, duration, filepath, mimetype));
            }
            while (cursor.moveToNext());
        if (cursor != null)
            cursor.close();
        return arrayList;
    }

    public static Cursor makeVideoCursor(Context context, String selection, String[] paramArrayOfString) {
        //final String vidSortOrder = PreferencesUtility.getInstance(context).getSongSortOrder();
        final String vidSortOrder = null;
        return makeVideoCursor(context, selection, paramArrayOfString, vidSortOrder);
    }

    private static Cursor makeVideoCursor(Context context, String selection, String[] paramArrayOfString, String sortOrder) {
        String selectionStatement = "title != ''";

        if (!TextUtils.isEmpty(selection)) {
            selectionStatement = selectionStatement + " AND " + selection;
        }

        String[] mediaColumns = {MediaStore.Video.Media._ID, MediaStore.Video.Media.TITLE, MediaStore.Video.Media.DURATION, MediaStore.Video.Media.DATA, MediaStore.Video.Media.MIME_TYPE};

        return context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, mediaColumns, selectionStatement, paramArrayOfString, sortOrder);

    }
}
