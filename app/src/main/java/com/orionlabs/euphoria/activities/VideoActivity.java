package com.orionlabs.euphoria.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.orionlabs.euphoria.R;

public class VideoActivity extends BaseThemedActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Videos");
    }
}
