package com.orionlabs.euphoria.models;

public class Video {
    public final long id;
    public final String videoTitle;
    public final int duration;
    public final String filepath;
    public final String mimetype;

    public Video() {
        this.id = -1;
        this.videoTitle = "";
        this.duration = -1;
        this.filepath = "";
        this.mimetype = "";
    }

    public Video(long _id, String _title, int _duration, String _filepath, String _mimetype) {
        this.id = _id;
        this.videoTitle = _title;
        this.duration = _duration;
        this.filepath = _filepath;
        this.mimetype = _mimetype;
    }
}
