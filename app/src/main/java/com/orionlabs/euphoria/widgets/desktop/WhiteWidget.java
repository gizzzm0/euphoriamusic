package com.orionlabs.euphoria.widgets.desktop;

import com.orionlabs.euphoria.R;

public class WhiteWidget extends StandardWidget {

    @Override
    int getLayoutRes() {
        return R.layout.widget_white;
    }
}
