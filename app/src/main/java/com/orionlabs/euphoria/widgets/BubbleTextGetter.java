package com.orionlabs.euphoria.widgets;

public interface BubbleTextGetter {
    String getTextToShowInBubble(int pos);
}